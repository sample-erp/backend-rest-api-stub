FROM adoptopenjdk/openjdk11:jdk-11.0.5_10-alpine as builder
ADD . /app
WORKDIR /app
RUN ./mvnw package -DskipTests

FROM adoptopenjdk:11-jre-hotspot-focal
ENV JAVA_HOME=/opt/java/openjdk
ENV PATH="$PATH:$JAVA_HOME/bin"
ENV JAVA_OPTS=""
WORKDIR /app
COPY --from=builder /app/target/rest-api-stub-*.jar rest-api-stub.jar
COPY .docker/start.sh .
RUN chmod +x start.sh
EXPOSE 8081
ENTRYPOINT ["./start.sh"]