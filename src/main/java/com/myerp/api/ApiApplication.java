package com.myerp.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class ApiApplication {

	static Logger logger = LoggerFactory.getLogger(ApiApplication.class);

	public static void main(String[] args) {
		ApplicationContext ctx =SpringApplication.run(ApiApplication.class, args);

		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			logger.debug(beanName);
		}
	}
}
