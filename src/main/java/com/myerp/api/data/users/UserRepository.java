package com.myerp.api.data.users;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin("*")
@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository  extends PagingAndSortingRepository<User, UUID> {
    List<User> findByUsername(@Param("username") String username);
    Optional<User> getByUsername(@Param("username") String username);
    Boolean existsByUsername(@Param("username") String username);
    Boolean existsByEmail(@Param("email") String email);
}
