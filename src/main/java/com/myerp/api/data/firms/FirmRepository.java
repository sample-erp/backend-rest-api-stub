package com.myerp.api.data.firms;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin("*")
@RepositoryRestResource(collectionResourceRel = "firms", path = "firms")
public interface FirmRepository extends PagingAndSortingRepository<Firm, UUID> {
    List<Firm> findByName(@Param("name") String name);
    Optional<Firm> getByInn(@Param("inn") String inn);
}
