package com.myerp.api.data.firms;

import com.myerp.api.data.users.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity

@Table (
        indexes = {
                @Index(name = "firm_name_idx", columnList = ("name"), unique = true)
        }
)
public class Firm {

    public static final int UNDEFINED = 0;
    public static final int LEGAL_TYPE = 2;         // юридическое лицо
    public static final int INDIVIDUAL_TYPE = 1;    // ИП

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private UUID uuid;
    private int type = UNDEFINED;
    // Наименование фирмы
    private String name;
    private String fullName;
    // Для ИП - данные предпринимателя
    private String firstName;
    private String lastName;
    private String patronymic;

    private String inn;
    private String kpp;
    private String jurAddress;
    private String postAddress;
    private String phones;
    private String email;
    private String managerFio;
    private String managerPosition;

    @ManyToMany
    private List<User> users = new ArrayList<>();

    protected Firm() {
    }

    public Firm(int type, String name, String fullName, String inn) {
        this.type = type;
        this.name = name;
        this.fullName = fullName;
        this.inn = inn;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getJurAddress() {
        return jurAddress;
    }

    public void setJurAddress(String jurAddress) {
        this.jurAddress = jurAddress;
    }

    public String getPostAddress() {
        return postAddress;
    }

    public void setPostAddress(String postAddress) {
        this.postAddress = postAddress;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getManagerFio() {
        return managerFio;
    }

    public void setManagerFio(String managerFio) {
        this.managerFio = managerFio;
    }

    public String getManagerPosition() {
        return managerPosition;
    }

    public void setManagerPosition(String managerPosition) {
        this.managerPosition = managerPosition;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Firm firm = (Firm) o;
        return type == firm.type && Objects.equals(uuid, firm.uuid) && Objects.equals(name, firm.name) && Objects.equals(fullName, firm.fullName) && Objects.equals(firstName, firm.firstName) && Objects.equals(lastName, firm.lastName) && Objects.equals(patronymic, firm.patronymic) && Objects.equals(inn, firm.inn) && Objects.equals(kpp, firm.kpp) && Objects.equals(jurAddress, firm.jurAddress) && Objects.equals(postAddress, firm.postAddress) && Objects.equals(phones, firm.phones) && Objects.equals(email, firm.email) && Objects.equals(managerFio, firm.managerFio) && Objects.equals(managerPosition, firm.managerPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, type, name, fullName, firstName, lastName, patronymic, inn, kpp, jurAddress, postAddress, phones, email, managerFio, managerPosition);
    }

    @Override
    public String toString() {
        return "Firm{" +
                "uuid=" + uuid +
                ", type=" + type +
                ", name='" + name + '\'' +
                ", fullName='" + fullName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", inn='" + inn + '\'' +
                ", kpp='" + kpp + '\'' +
                ", jurAddress='" + jurAddress + '\'' +
                ", postAddress='" + postAddress + '\'' +
                ", phones='" + phones + '\'' +
                ", email='" + email + '\'' +
                ", managerFio='" + managerFio + '\'' +
                ", managerPosition='" + managerPosition + '\'' +
                '}';
    }
}
