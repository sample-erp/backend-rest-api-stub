package com.myerp.api.data.accounts;

import com.myerp.api.data.firms.Firm;
import com.myerp.api.data.users.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity

@Table (
        indexes = {
                @Index(name = "account_name_idx", columnList = ("name"), unique = true)
        }
)
public class Account {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private UUID uuid;
    private String name;
    private String description;

    @OneToMany
    private List<User> users = new ArrayList<>();
    @OneToMany
    private List<Firm> firms = new ArrayList<>();

    protected Account() {
    }

    public Account(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Firm> getFirms() {
        return firms;
    }

    public void setFirms(List<Firm> firms) {
        this.firms = firms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(uuid, account.uuid) && Objects.equals(name, account.name) && Objects.equals(description, account.description) && Objects.equals(users, account.users) && Objects.equals(firms, account.firms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, name, description, users, firms);
    }

    @Override
    public String toString() {
        return "Account{" +
                "uuid=" + uuid +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", users=" + users +
                ", firms=" + firms +
                '}';
    }
}
