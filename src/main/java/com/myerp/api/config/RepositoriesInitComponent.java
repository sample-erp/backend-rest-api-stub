package com.myerp.api.config;

import com.myerp.api.data.accounts.Account;
import com.myerp.api.data.accounts.AccountRepository;
import com.myerp.api.data.firms.Firm;
import com.myerp.api.data.firms.FirmRepository;
import com.myerp.api.data.roles.Role;
import com.myerp.api.data.roles.RoleRepository;
import com.myerp.api.data.users.User;
import com.myerp.api.data.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class RepositoriesInitComponent {

    private AccountRepository accountRepository;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private FirmRepository firmRepository;
    private PasswordEncoder encoder;

    @Autowired
    public RepositoriesInitComponent(AccountRepository accountRepository,
                                     FirmRepository firmRepository,
                                     UserRepository userRepository,
                                     RoleRepository roleRepository,
                                     PasswordEncoder encoder) {

        this.accountRepository = accountRepository;
        this.firmRepository = firmRepository;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.encoder = encoder;

        initRoles();
        initUsers();
        initFirms();
        initAccounts();
    }

    protected void initRoles() {
        roleRepository.save(new Role("ROLE_ADMIN", "ADMIN", "Администратор системы"));
        roleRepository.save(new Role("ROLE_DEPOT", "DEPOT", "Работник склада"));
        roleRepository.save(new Role("ROLE_SALE", "SALE", "Продавец"));
        roleRepository.save(new Role("ROLE_DOCTOR", "DOCTOR", "Офтальмолог"));
    }

    protected void initUsers() {

        addUser("serg", "123","Сергей", "Ткаченко",
                new String[] {"ROLE_ADMIN", "ROLE_SALE"});

        addUser("marina", "456", "Марина", "Волошко",
                new String[] {"ROLE_ADMIN"});

        for(int i=0; i < 50; i++) {
            String formatted = String.format("%03d", i);
            userRepository.save(new User("login-" + formatted, encoder.encode("456"),
                    "first-name-" + formatted, "last-name-" + formatted));
        }
    }

    @Transactional
    protected void addUser(String name, String password,
                           String firstName, String lastName, String[] roleIds) {

        List<Role> roles = new ArrayList<>();
        for(String id : roleIds) {
            Optional<Role> optionalRole = roleRepository.findById(id);
            if(optionalRole.isPresent())
                roles.add(optionalRole.get());
        }

        User user = new User(name, encoder.encode(password), firstName, lastName);
        user.setRoles(roles);
        userRepository.save(user);
    }

    protected void initFirms() {

        addFirm(Firm.LEGAL_TYPE, "ООО \"Лютик\"",
                "Общество с ограниченной ответственностью \"Лютик\"",
                "7707083893", new String[] {"serg"});

        addFirm(Firm.INDIVIDUAL_TYPE, "ИП Волошко М.Ю.",
                "Индивидуальный предприниматель Волошко Марина Юрьевна",
                "7707082000", new String[] {"marina"});

        for(int i=0; i < 25; i++) {
            String formatted = String.format("%03d", i);
            firmRepository.save(new Firm(Firm.LEGAL_TYPE,"name-" + formatted,
                    "fullname-" + formatted, "inn-" + formatted));
        }
    }

    @Transactional
    protected void addFirm(int type, String name, String fullName, String inn, String[] userNames) {

        List<User> users = new ArrayList<>();
        for(String userName : userNames) {
            Optional<User> optionalUser = userRepository.getByUsername(userName);
            if(optionalUser.isPresent())
                users.add(optionalUser.get());
        }

        Firm firm = new Firm(type, name,fullName, inn);

        firm.setUsers(users);
        firmRepository.save(firm);
    }

    protected void initAccounts() {

        addAccount("Маринэ Продюсьен", "Кодирую. От запоя и печенек.",
                new String[] {"7707082000"}, new String[] {"marina"});

        addAccount("Serg Corporation", "Стартапим по-маленьку",
                new String[] {"7707083893"}, new String[] {"serg"});

        for(int i=0; i < 15; i++) {
            String formatted = String.format("%03d", i);
            accountRepository.save(new Account("name-" + formatted, "description-" + formatted));
        }
    }

    @Transactional
    protected void addAccount(String name, String description, String[] firmInns, String[] userNames) {

        List<Firm> firms = new ArrayList<>();
        for(String inn : firmInns) {
            Optional<Firm> optionalFirm = firmRepository.getByInn(inn);
            if(optionalFirm.isPresent())
                firms.add(optionalFirm.get());
        }

        List<User> users = new ArrayList<>();
        for(String userName : userNames) {
            Optional<User> optionalUser = userRepository.getByUsername(userName);
            if(optionalUser.isPresent())
                users.add(optionalUser.get());
        }

        Account account = new Account(name, description);
        account.setFirms(firms);
        account.setUsers(users);
        accountRepository.save(account);
    }
}
