REST API Stub application. Uses Spring Boot & H2 Database & Embedded Tomcat.

**Building**

To build by Maven tool go to project dir and print in console:  
`mvn clean package`

Or open Maven project in your favorite IDE and run build from it.

Result JAR contains Embedded Tomcat and emulates REST API endpoints stubs.

**Run**

`java -jar rest-api-stub-0.1.jar`

This command runs application on port `8081` and `/api/v1/` path.

**Request examples**

Get available endpoints: `http://localhost:8081/api/v1`

Get user list: `http://localhost:8081/api/v1/users`

Get user list sorting by firstName desc:
`http://localhost:8081/api/v1/users?sort=firstName,desc`

# Работа с Docker
## Создание образа
`docker build -t optic-erp/backend-stub .`

## Запуск контейнера
Предварительно создаем и заполняем файл с переменными окружения .env по шаблону [template.env](.docker/template.env).

Запускаем контейнер:

`docker run --env-file .env -d -p 8081:8081 optic-erp/backend-stub`

## Deployment with docker-compose

1. В `docker-compose.yml` объявляем сервис на основе локального образа `optic-erp/backend-stub`
   или образа из удаленного реестра, если таковой используется.


2. Создаем и заполняем файл с переменными окружения .env по шаблону [template.env](.docker/template.env).


3. В `docker-compose.yml` определяем `$JAVA_OPTS` с нужными опциями (секция `environment`):

```yaml
environment:
  JAVA_OPTS:
         -XX:MinRAMPercentage=75.0 
         -XX:MaxRAMPercentage=75.0
         -XX:+UseG1GC
```

4. При желании монтируем локальную директорию для доступа к файлам логов
   (необходимо иметь права на запись/чтение в эту директорию):

```yaml
volumes:
  - "/host/path/to/logs/:/app/.logs/"
```

Пример docker-compose.yml:
``` yaml
version: '3.9'
services:
  backend:
    image: optic-erp/backend-stub
    restart: always
    env_file:
      - .env
    ports:
      - "8081:8081"
    environment:
      JAVA_OPTS:
         -XX:MinRAMPercentage=75.0 
         -XX:MaxRAMPercentage=75.0
         -XX:+UseG1GC
    volumes:
      - "/host/path/to/logs/:/app/.logs/"
```